<?php

class BookException extends Exception{}
// String id,
// String judulBuku,
// String deskripsi,
// int harga,
// String gambar,
// int pemilikId,
// int peminjamId,
// int status,
// bool isPublish,
class Book{
    private $id;
    private $judulBuku;
    private $deskripsi;
    private $harga;
    private $gambar;
    private $genreId;
    private $genreName;
    private $pemilikId;
    private $pemilikName;
    private $peminjamId;
    private $peminjamName;
    private $status;
    private $isPublish;

    public function __construct( 
        $id,
         $judulBuku,
         $deskripsi,
         $harga,
         $gambar,
         $genreId,
         $genreName,
         $pemilikId,
         $pemilikName,
         $peminjamId,
         $peminjamName,
         $status,
         $isPublish)
    {
        $this->setId($id);
        $this->setJudulBuku($judulBuku);
        $this->setDeskripsi($deskripsi);
        $this->setHarga($harga);
        $this->setGambar($gambar);
        $this->setGenreId($genreId);
        $this->setGenreName($genreName);
        $this->setPemilikId($pemilikId);
        $this->setPemilikName($pemilikName);
        $this->setPeminjamId($peminjamId);
        $this->setPeminjamName($peminjamName);
        $this->setStatus($status);
        $this->setIsPublish($isPublish);

    }
    
    function returnAsArray(){
        $book = array();
        $book['id'] = $this->getId();
        $book['judulBuku'] = $this->getJudulBuku();
        $book['deskripsi'] = $this->getDeskripsi();
        $book['harga'] = $this->getHarga();
        $book['gambar'] = $this->getGambar();
        $book['genreId'] = $this->getGenreId();
        $book['genreName'] = $this->getGenreName();
        $book['pemilikId'] = $this->getPemilikId();
        $book['pemilikName'] = $this->getPemilikName();
        $book['peminjamId'] = $this->getPeminjamId();
        $book['peminjamName'] = $this->getPeminjamName();
        $book['status'] = $this->getStatus();
        $book['isPublish'] = $this->getIsPublish();
        return $book;
    }



    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of judulBuku
     */ 
    public function getJudulBuku()
    {
        return $this->judulBuku;
    }

    /**
     * Set the value of judulBuku
     *
     * @return  self
     */ 
    public function setJudulBuku($judulBuku)
    {
        $this->judulBuku = $judulBuku;

        return $this;
    }

    /**
     * Get the value of deskripsi
     */ 
    public function getDeskripsi()
    {
        return $this->deskripsi;
    }

    /**
     * Set the value of deskripsi
     *
     * @return  self
     */ 
    public function setDeskripsi($deskripsi)
    {
        $this->deskripsi = $deskripsi;

        return $this;
    }

    /**
     * Get the value of harga
     */ 
    public function getHarga()
    {
        return $this->harga;
    }

    /**
     * Set the value of harga
     *
     * @return  self
     */ 
    public function setHarga($harga)
    {
        $this->harga = $harga;

        return $this;
    }

    /**
     * Get the value of gambar
     */ 
    public function getGambar()
    {
        return $this->gambar;
    }

    /**
     * Set the value of gambar
     *
     * @return  self
     */ 
    public function setGambar($gambar)
    {
        $this->gambar = $gambar;

        return $this;
    }

    /**
     * Get the value of pemilikId
     */ 
    public function getPemilikId()
    {
        return $this->pemilikId;
    }

    /**
     * Set the value of pemilikId
     *
     * @return  self
     */ 
    public function setPemilikId($pemilikId)
    {
        $this->pemilikId = $pemilikId;

        return $this;
    }

    /**
     * Get the value of peminjamId
     */ 
    public function getPeminjamId()
    {
        return $this->peminjamId;
    }

    /**
     * Set the value of peminjamId
     *
     * @return  self
     */ 
    public function setPeminjamId($peminjamId)
    {
        $this->peminjamId = $peminjamId;

        return $this;
    }

    /**
     * Get the value of status
     */ 
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set the value of status
     *
     * @return  self
     */ 
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get the value of isPublish
     */ 
    public function getIsPublish()
    {
        return $this->isPublish;
    }

    /**
     * Set the value of isPublish
     *
     * @return  self
     */ 
    public function setIsPublish($isPublish)
    {
        $this->isPublish = $isPublish;

        return $this;
    }

    /**
     * Get the value of genreId
     */ 
    public function getGenreId()
    {
        return $this->genreId;
    }

    /**
     * Set the value of genreId
     *
     * @return  self
     */ 
    public function setGenreId($genreId)
    {
        $this->genreId = $genreId;

        return $this;
    }

    /**
     * Get the value of genreName
     */ 
    public function getGenreName()
    {
        return $this->genreName;
    }

    /**
     * Set the value of genreName
     *
     * @return  self
     */ 
    public function setGenreName($genreName)
    {
        $this->genreName = $genreName;

        return $this;
    }

    /**
     * Get the value of pemilikName
     */ 
    public function getPemilikName()
    {
        return $this->pemilikName;
    }

    /**
     * Set the value of pemilikName
     *
     * @return  self
     */ 
    public function setPemilikName($pemilikName)
    {
        $this->pemilikName = $pemilikName;

        return $this;
    }

    /**
     * Get the value of peminjamName
     */ 
    public function getPeminjamName()
    {
        return $this->peminjamName;
    }

    /**
     * Set the value of peminjamName
     *
     * @return  self
     */ 
    public function setPeminjamName($peminjamName)
    {
        $this->peminjamName = $peminjamName;

        return $this;
    }
}