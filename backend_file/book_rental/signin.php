<?php
header("Content-Type: application/json; charset=UTF-8");
require_once('database.php');
require_once('response.php');

try {
    $db = Database::getConnection();
} catch (PDOException $ex) {
    sendResponse(500,false,"Error Connecting to Database");
}

//LOGIN
if(empty($_GET)){
    if($_SERVER['REQUEST_METHOD'] != 'POST') {
        sendResponse(400,false,"Request Method not Allowed");
    }
    //delay
    sleep(1);

    if($_SERVER['CONTENT_TYPE'] !== 'application/json') {
        sendResponse(400,false,"Content Type header not set to JSON");
    }

    $data = file_get_contents("php://input");

    if(!json_decode($data)){
        sendResponse(400,false,"Request body is not valid JSON");
    }

    $jsonData = json_decode($data);

    if(!isset($jsonData->email) || !isset($jsonData->password)){
        sendResponse(400,false,"Email / Password is Empty");
    }

    try{
        $email = $jsonData->email;
        $password = $jsonData->password;
        $query = $db->prepare("SELECT ID, nama, email, no_telepon,alamat,password,roles_id FROM user WHERE email = :email");
        $query->bindParam(":email",$email, PDO::PARAM_STR);
        $query->execute();
        $rowCount = $query->rowCount();
        if($rowCount===0){
            sendResponse(400,false,"Email not registered");
        }
        $result = $query->fetch(PDO::FETCH_ASSOC);
        $userId = $result["ID"];
        $passwordResult = $result["password"];
        //Expiration date by day

        if(password_verify($password, $passwordResult)){
            $responseData = array();
            $responseData["ID"] = $result["ID"];
            $responseData["nama"] = $result["nama"];
            $responseData["email"] = $result["email"];
            $responseData["no_telepon"] = $result["no_telepon"];
            $responseData["alamat"] = $result["alamat"];
            $responseData["roles_id"] = $result["roles_id"];
            sendResponse(201,true,"Success",$responseData);
        }
        else{
            sendResponse(401,false,"Incrorrect Email/Password");
        }

    }catch(Exception $ex){
        sendResponse(400,false,"Error Sign In");
    }
}

