<?php 
    class Database {
        private static $host = "localhost";
        private static $database_name = "book_rental";
        private static $username = "root";
        private static $password = "";
        public static $conn;

        public static function getConnection(){
            if(self::$conn == null){
                self::$conn = new PDO('mysql:host='.self::$host.';dbname='.self::$database_name.';charset=utf8', self::$username, self::$password);
                self::$conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            }
            return self::$conn;
        }
    }
      
