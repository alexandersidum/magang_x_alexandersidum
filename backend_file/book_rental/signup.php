<?php
header("Content-Type: application/json; charset=UTF-8");
require_once('database.php');
require_once('response.php');

try {
    $db = Database::getConnection();
} catch (PDOException $ex) {
  sendResponse(500, false , "Error Connecting to Database".$ex->getMessage());
}

if($_SERVER['REQUEST_METHOD'] !== 'POST') {
  sendResponse(405, false , "Request method not allowed");
}

  if($_SERVER['CONTENT_TYPE'] !== 'application/json') {
    sendResponse(400,false,"Content Type header not set to JSON");
  }
  $data = file_get_contents("php://input");
  
  if(!$jsonData = json_decode($data)) {
    sendResponse(400, false , "Request body is not valid JSON");
  }

  try{
    $db->beginTransaction();
    signUpUser($jsonData , $db);
    $db->commit();
    sendResponse(200, true , "Success");
  } catch(Exception $ex){
    $db->rollBack();
    sendResponse(400, false , "Error Signing Up");
  }

// ID, nama, email, no_telpon, alamat, password, roles_id
function signUpUser($jsonData , $db){
  try{
    $query = $db->prepare("INSERT INTO user(nama, email, no_telepon,alamat,password,roles_id) 
    VALUES(:nama, :email, :no_telepon, :alamat, :password, :roles_id)");
    $hashed_password = password_hash($jsonData->password, PASSWORD_DEFAULT);
    $query->bindParam(':email', $jsonData->email, PDO::PARAM_STR);
    $query->bindParam(':nama', $jsonData->nama, PDO::PARAM_STR);
    $query->bindParam(':password', $hashed_password, PDO::PARAM_STR);
    $query->bindParam(':roles_id', $jsonData->roles_id, PDO::PARAM_INT);
    $query->bindParam(':no_telepon', $jsonData->no_telepon, PDO::PARAM_STR);
    $query->bindParam(':alamat', $jsonData->alamat, PDO::PARAM_STR);
    $query->execute();
    $rowCount = $query->rowCount();
    if($rowCount<1){
      throw("Failed to Register / Signup");
    }
  }catch(Exception $ex){
    $db->rollBack();
    sendResponse(400, false , "Error Signing Up".$ex->getMessage());
  }
};



