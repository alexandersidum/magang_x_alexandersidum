<?php

class Response
{
    private $_statusCode;
    private $_message = array();
    private $_data;
    private $_isSuccess;
    private $_responseData = array();
    private $_toCache = false;


    function setStatusCode($newStatusCode)
    {
        $this->_statusCode = $newStatusCode;
    }

    function setData($newData)
    {
        $this->_data = $newData;
    }

    function setIsSuccess($newIsSuccess)
    {
        $this->_isSuccess = $newIsSuccess;
    }

    function setToCache($newToCache)
    {
        $this->_toCache = $newToCache;
    }

    function addMessage($newMessage)
    {
        $this->_message[] = $newMessage;
    }

    function send()
    {
        header('Content-type:application/json;charset=utf-8');
        if ($this->_toCache == true) {
            header('Cache-Control: max-age=60');
        } else {
            header('Cache-Control: no-cache, no-store');
        }

        if ($this->_statusCode == 500 || !is_numeric($this->_statusCode)) {
            http_response_code(500);
            $this->_responseData['statusCode'] = 500;
            $this->_responseData['isSuccess'] = false;
            $this->addMessage("Response creation error");
            $this->_responseData['messages'] = $this->_message;
        } else {
            http_response_code($this->_statusCode);
            $this->_responseData['statusCode'] = $this->_statusCode;
            $this->_responseData['isSuccess'] = $this->_isSuccess;
            $this->_responseData['messages'] = $this->_message;
            $this->_responseData['data'] = $this->_data;
        }

        echo json_encode($this->_responseData);
    }
}

function sendResponse($statusCode, $isSuccess, $message = null, $data = null)
{
    $response = new Response();
    $response->setStatusCode($statusCode);
    $response->setIsSuccess($isSuccess);
    if ($message != null) {
        $response->addMessage($message);
    }
    if ($data != null) {
        $response->setData($data);
    }
    $response->send();
    exit;
}
