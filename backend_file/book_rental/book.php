<?php
header("Content-Type: application/json; charset=UTF-8");
require_once('database.php');
require_once('response.php');
require_once('./model/book.php');

try {
    $db = Database::getConnection();
} catch (PDOException $ex) {
    sendResponse(500, false, "Error Connecting to Database");
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    // check request's content type header is form data
    if (!isset($_SERVER['CONTENT_TYPE']) || strpos($_SERVER['CONTENT_TYPE'], "multipart/form-data; boundary=") === false) {

        sendResponse(400, false, "Content Type header not set to Form Data " . $_SERVER['CONTENT_TYPE']);
    }

    if (!$jsonData = json_decode($_POST["data"])) {
        sendResponse(400, false, "Request body is not valid JSON");
    }
    $jsonData = json_decode($_POST["data"]);
    $image = $_FILES["image"] ?? null;

    //IF BOOK ID SUDAH DIBERIKAN BERARTI EDIT BOOK
    if (isset($jsonData->bookId)) {
        $bookId = $jsonData->bookId;
        $product = getSingleBook($bookId, $db);
        try {
            $bookId  = $jsonData->bookId;
            //GET CURRENT PRODUK INFO
            $currentBook = getSingleBook($bookId, $db);
            $db->beginTransaction();
            $query = $db->prepare("UPDATE books SET judul_buku = :judulBuku , deskripsi = :deskripsi, harga = :harga , 
            genre_id = :genreId, is_publish = :isPublish , peminjam_id = :peminjamId where ID = :bookId");
            $query->bindParam(":judulBuku", $jsonData->judulBuku, PDO::PARAM_STR);
            $query->bindParam(":deskripsi", $jsonData->deskripsi, PDO::PARAM_STR);
            $query->bindParam(":harga", $jsonData->harga, PDO::PARAM_INT);
            $query->bindParam(":genreId", $jsonData->genreId, PDO::PARAM_INT);
            $query->bindParam(":isPublish", $jsonData->isPublish, PDO::PARAM_INT);
            $query->bindParam(":peminjamId", $jsonData->peminjamId, PDO::PARAM_INT);
            $query->bindParam(":bookId", $bookId, PDO::PARAM_INT);
            $query->execute();
            $rowCount = $query->rowCount();
            saveBookImage($image, $currentBook, $db);
            $db->commit();
            sendResponse(201, true, "Produk Info Updated");
        } catch (Exception $ex) {
            $db->rollBack();
            sendResponse(400, false, "Error Fetching Product" . $ex->getMessage());
        }
    }
    //CREATE NEW BOOK----------------------------------------------------------------------
    //CREATE NEW BOOK HARUS ADA IMAGE 1
    if (!isset($_FILES["image"])) {
        //Tidak boleh tanpa gambar 1
        sendResponse(400, false, "No Image Attached");
    }
    try {
        $defaultStatus = 0;
        $db->beginTransaction();
        $query = $db->prepare("INSERT INTO books (genre_id, judul_buku, deskripsi, harga, pemilik_id, status, is_publish) 
      VALUES(:genreId , :judulBuku , :deskripsi, :harga, :pemilikId , :status , :isPublish)");
        $query->bindParam(":genreId", $jsonData->genreId, PDO::PARAM_INT);
        $query->bindParam(":judulBuku", $jsonData->judulBuku, PDO::PARAM_STR);
        $query->bindParam(":deskripsi", $jsonData->deskripsi, PDO::PARAM_STR);
        $query->bindParam(":harga", $jsonData->harga, PDO::PARAM_INT);
        $query->bindParam(":pemilikId", $jsonData->pemilikId, PDO::PARAM_INT);
        $query->bindParam(":status", $defaultStatus, PDO::PARAM_INT);
        $query->bindParam(":isPublish", $jsonData->isPublish, PDO::PARAM_INT);
        $query->execute();
        $rowCount = $query->rowCount();
        if ($rowCount < 1) {
            throw new Exception("Failed Insert Book");
        }
        $query = $db->query("SELECT ID FROM books WHERE ID = LAST_INSERT_ID()");
        $query->execute();
        $rowCount = $query->rowCount();
        if ($rowCount === 0) {
            throw new Exception("Failed Insert Book");
        }
        $bookId = $query->fetch(PDO::FETCH_ASSOC)["ID"];
        $book = getSingleBook($bookId, $db);
        saveBookImage($image, $book, $db);
        $db->commit();
        sendResponse(201, true, "Product Creation Success");
    } catch (Exception $ex) {
        $db->rollBack();
        sendResponse(400, false, "Error Creating New Book" . $ex->getMessage());
    }
};

//DELETE BOOK-----------------------------------------------------------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'DELETE') {
    try {
        if ($_SERVER['CONTENT_TYPE'] !== 'application/json') {
            sendResponse(400, false, "Content Type header not set to JSON");
        }

        $data = file_get_contents("php://input");

        if (!json_decode($data)) {
            sendResponse(400, false, "Request body is not valid JSON");
        }

        $jsonData = json_decode($data);

        if (!isset($jsonData->id)) {
            sendResponse(401, false, "Book ID not specified");
        }
        $bookId = $jsonData->id;
        $sql = "DELETE FROM books WHERE ID = :bookID";
        $query = $db->prepare($sql);
        $query->bindParam(":bookID", $bookId, PDO::PARAM_INT);
        $query->execute();
        if ($query->rowCount() < 1) {
            sendResponse(400, false, "Error Deleting Book");
        } else {
            sendResponse(201, true, "Success");
        }
    } catch (Exception $e) {
        sendResponse(400, false, "Error Deleting Book" . $e->getMessage());
    }
}


//GET BOOK-----------------------------------------------------------------------------------------
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    //GET Single Book
    if (isset($_GET['id'])) {
        try {
            $bookId = $_GET['id'];
            $book = getSingleBook($bookId, $db);
            sendResponse(201, true, "Success", $book->returnAsArray());
        } catch (Exception $ex) {
            sendResponse(400, false, "Error Fetching Book");
        }
    }

    if (isset($_GET['genre'])) {
        try {
            $query = $db->query("SELECT ID, genre FROM genre");
            $query->execute();
            if ($query->rowCount() === 0) {
                sendResponse(400, false, "Genre Tidak Ditemukan", $data = null);
            }
            $result = array();
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $genre = array();
                $genre['ID'] = $row["ID"];
                $genre['genre'] = $row["genre"];
                $result[] = $genre;
            }
            sendResponse(201, true, "Success", $result);
        } catch (Exception $ex) {
            sendResponse(400, false, "Error Fetching Book");
        }
    }

    //GET PRODUK BY PEMINJAM
    if (isset($_GET['peminjamId'])) {
        try {
            $peminjamId = $_GET['peminjamId'];
            if (isset($_GET['status'])) {
                $status = $_GET['status'];
                $sql = "SELECT books.ID, judul_buku, deskripsi, gambar, genre_id, harga, is_publish, pemilik_id, peminjam_id, genre.genre as genre_name,  status  FROM books, genre WHERE books.peminjam_id = :peminjamId and genre.ID = books.genre_id and books.status = :status";
                $query = $db->prepare($sql);
                $query->bindParam(":peminjamId", $peminjamId, PDO::PARAM_INT);
                $query->bindParam(":status", $status, PDO::PARAM_INT);
            } else {
                $sql = "SELECT books.ID, judul_buku, deskripsi, gambar, genre_id, harga, is_publish, pemilik_id, peminjam_id, genre.genre as genre_name,  status  FROM books, genre WHERE books.peminjam_id = :peminjamId and genre.ID = books.genre_id";
                $query = $db->prepare($sql);
                $query->bindParam(":peminjamId", $peminjamId, PDO::PARAM_INT);
            }

            $query->execute();
            if ($query->rowCount() === 0) {
                sendResponse(400, false, "Buku Tidak Ditemukan", $data = null);
            }
            $result = array();
            //Ambil semua data produk
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $pemilikId = $row["pemilik_id"];
                $peminjamId = $row["peminjam_id"];
                $queryPemilik = $db->prepare("SELECT nama FROM user where ID = :pemilikId");
                $queryPeminjam = $db->prepare("SELECT nama FROM user where ID = :peminjamId");
                $queryPemilik->bindParam(":pemilikId", $pemilikId, PDO::PARAM_INT);
                $queryPeminjam->bindParam(":peminjamId", $peminjamId, PDO::PARAM_INT);
                $queryPemilik->execute();
                $queryPeminjam->execute();
                $rowCount1 = $queryPemilik->rowCount();
                $rowCount2 = $queryPeminjam->rowCount();
                if (isset($peminjamId)) {
                    if ($rowCount1 === 0 || $rowCount2 === 0) {
                        sendResponse(400, false, "Error Geting User Data");
                    }
                } else {
                    if ($rowCount1 === 0) {
                        sendResponse(400, false, "Error Geting User Data");
                    }
                }
                $rowPemilik = $queryPemilik->fetch(PDO::FETCH_ASSOC);
                $rowPeminjam = $queryPeminjam->fetch(PDO::FETCH_ASSOC);
                $book = rowToBook($row, $rowPemilik, $rowPeminjam);
                $result[] = $book->returnAsArray();
            }
            sendResponse(201, true, "Success", $result);
        } catch (Exception $e) {
            sendResponse(400, false, "Error Fetching Book Data");
        }
    }

    //GET PRODUK BY PEMILIK
    if (isset($_GET['pemilikId'])) {
        try {
            $pemilikId = $_GET['pemilikId'];
            if (isset($_GET['status'])) {
                $status = $_GET['status'];
                $sql = "SELECT books.ID, judul_buku, deskripsi, gambar, genre_id, harga, is_publish, pemilik_id, peminjam_id, genre.genre as genre_name,  status  FROM books, genre WHERE books.pemilik_id = :pemilikId and genre.ID = books.genre_id and books.status = :status";
                $query = $db->prepare($sql);
                $query->bindParam(":pemilikId", $pemilikId, PDO::PARAM_INT);
                $query->bindParam(":status", $status, PDO::PARAM_INT);
            } else {
                $sql = "SELECT books.ID, judul_buku, deskripsi, gambar, genre_id, harga, is_publish, pemilik_id, peminjam_id, genre.genre as genre_name,  status  FROM books, genre WHERE books.pemilik_id = :pemilikId and genre.ID = books.genre_id";
                $query = $db->prepare($sql);
                $query->bindParam(":pemilikId", $pemilikId, PDO::PARAM_INT);
            }

            $query->execute();
            if ($query->rowCount() === 0) {
                sendResponse(400, false, "Buku Tidak Ditemukan", $data = null);
            }
            $result = array();
            //Ambil semua data produk
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $pemilikId = $row["pemilik_id"];
                $peminjamId = $row["peminjam_id"];
                $queryPemilik = $db->prepare("SELECT nama FROM user where ID = :pemilikId");
                $queryPeminjam = $db->prepare("SELECT nama FROM user where ID = :peminjamId");
                $queryPemilik->bindParam(":pemilikId", $pemilikId, PDO::PARAM_INT);
                $queryPeminjam->bindParam(":peminjamId", $peminjamId, PDO::PARAM_INT);
                $queryPemilik->execute();
                $queryPeminjam->execute();
                $rowCount1 = $queryPemilik->rowCount();
                $rowCount2 = $queryPeminjam->rowCount();
                if (isset($peminjamId)) {
                    if ($rowCount1 === 0 || $rowCount2 === 0) {
                        sendResponse(400, false, "Error Geting User Data");
                    }
                } else {
                    if ($rowCount1 === 0) {
                        sendResponse(400, false, "Error Geting User Data");
                    }
                }
                $rowPemilik = $queryPemilik->fetch(PDO::FETCH_ASSOC);
                $rowPeminjam = $queryPeminjam->fetch(PDO::FETCH_ASSOC);
                $book = rowToBook($row, $rowPemilik, $rowPeminjam);
                $result[] = $book->returnAsArray();
            }
            sendResponse(201, true, "Success", $result);
        } catch (Exception $e) {
            sendResponse(400, false, "Error Fetching Book Data");
        }
    }

    //GET PUBLISHED BOOK
    if (isset($_GET['isPublished'])) {
        try {
            $isPublished = $_GET['isPublished'];
            $sql = "SELECT books.ID, judul_buku, deskripsi, gambar, genre_id, harga, is_publish, pemilik_id, peminjam_id, genre.genre as genre_name,  status  FROM books, genre WHERE genre.ID = books.genre_id and books.is_publish = :isPublished";
            $query = $db->prepare($sql);
            $query->bindParam(":isPublished", $isPublished, PDO::PARAM_INT);

            $query->execute();
            if ($query->rowCount() === 0) {
                sendResponse(400, false, "Buku Tidak Ditemukan", $data = null);
            }
            $result = array();
            //Ambil semua data produk
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $pemilikId = $row["pemilik_id"];
                $peminjamId = $row["peminjam_id"];
                $queryPemilik = $db->prepare("SELECT nama FROM user where ID = :pemilikId");
                $queryPeminjam = $db->prepare("SELECT nama FROM user where ID = :peminjamId");
                $queryPemilik->bindParam(":pemilikId", $pemilikId, PDO::PARAM_INT);
                $queryPeminjam->bindParam(":peminjamId", $peminjamId, PDO::PARAM_INT);
                $queryPemilik->execute();
                $queryPeminjam->execute();
                $rowCount1 = $queryPemilik->rowCount();
                $rowCount2 = $queryPeminjam->rowCount();
                if (isset($peminjamId)) {
                    if ($rowCount1 === 0 || $rowCount2 === 0) {
                        sendResponse(400, false, "Error Geting User Data");
                    }
                } else {
                    if ($rowCount1 === 0) {
                        sendResponse(400, false, "Error Geting User Data");
                    }
                }
                $rowPemilik = $queryPemilik->fetch(PDO::FETCH_ASSOC);
                $rowPeminjam = $queryPeminjam->fetch(PDO::FETCH_ASSOC);
                $book = rowToBook($row, $rowPemilik, $rowPeminjam);
                $result[] = $book->returnAsArray();
            }
            sendResponse(201, true, "Success", $result);
        } catch (Exception $e) {
            sendResponse(400, false, "Error Fetching Book Data" . $e->getMessage());
        }
    }
}



function saveBookImage($image, $book, $db)
{
    $bookId = $book->getId();
    $dir = "images/book/" . $bookId . "/";
    $imageExt = pathinfo($image['name'], PATHINFO_EXTENSION) ?? "";
    if (!is_dir($dir)) {
        if (!mkdir($dir)) {
            throw new Exception("Failed Uploading image");
        }
    }
    if (isset($image)) {
        if (!move_uploaded_file($image["tmp_name"], $dir . $bookId . "img." . $imageExt)) {
            throw new Exception("Failed Uploading image");
        }
    }

    $imageUrl = isset($image) ? "images/book/" . strval($bookId) . "/" . $bookId . "img." . $imageExt : $book->getGambar();
    $query = $db->prepare("UPDATE books SET gambar = :imageUrl WHERE ID = :bookId");
    $query->bindParam(":imageUrl", $imageUrl, PDO::PARAM_STR);
    $query->bindParam(":bookId", $bookId, PDO::PARAM_INT);
    $query->execute();
}

function getSingleBook($bookId, $db)
{
    $query = $db->prepare("SELECT books.ID, judul_buku, deskripsi, gambar, genre_id, harga, is_publish, pemilik_id, peminjam_id, genre.genre as genre_name,  status  FROM books, genre WHERE books.ID = :bookId and genre.ID = books.genre_id");
    $query->bindParam(":bookId", $bookId, PDO::PARAM_INT);
    $query->execute();
    if ($query->rowCount() === 0) {
        sendResponse(400, false, "Invalid Book Id", $data = null);
    }
    $row = $query->fetch(PDO::FETCH_ASSOC);
    $pemilikId = $row["pemilik_id"];
    $peminjamId = $row["peminjam_id"];
    $queryPemilik = $db->prepare("SELECT nama FROM user where ID = :pemilikId");
    $queryPeminjam = $db->prepare("SELECT nama FROM user where ID = :peminjamId");
    $queryPemilik->bindParam(":pemilikId", $pemilikId, PDO::PARAM_INT);
    $queryPeminjam->bindParam(":peminjamId", $peminjamId, PDO::PARAM_INT);
    $queryPemilik->execute();
    $queryPeminjam->execute();
    $rowCount1 = $queryPemilik->rowCount();
    $rowCount2 = $queryPeminjam->rowCount();
    if (isset($peminjamId)) {
        if ($rowCount1 === 0 || $rowCount2 === 0) {
            sendResponse(400, false, "Error Geting User Data");
        }
    } else {
        if ($rowCount1 === 0) {
            sendResponse(400, false, "Error Geting User Data");
        }
    }
    $rowPemilik = $queryPemilik->fetch(PDO::FETCH_ASSOC);
    $rowPeminjam = $queryPeminjam->fetch(PDO::FETCH_ASSOC);
    $book = rowToBook($row, $rowPemilik, $rowPeminjam);
    return $book;
}

function rowToBook($row, $rowPemilik, $rowPeminjam)
{
    $id = $row["ID"];
    $judulBuku = $row["judul_buku"];
    $deskripsi = $row["deskripsi"];
    $harga = $row["harga"];
    $gambar = $row["gambar"];
    $genreId = $row["genre_id"];
    $genreName = $row["genre_name"];
    $pemilikId = $row["pemilik_id"];
    $pemilikName = $rowPemilik["nama"];
    $peminjamId = $row["peminjam_id"];
    $peminjamName = $rowPeminjam["nama"];
    $status = $row["status"];
    $isPublish = $row["is_publish"];
    $book = new Book(
        $id,
        $judulBuku,
        $deskripsi,
        $harga,
        $gambar,
        $genreId,
        $genreName,
        $pemilikId,
        $pemilikName,
        $peminjamId,
        $peminjamName,
        $status,
        $isPublish
    );
    return $book;
}
