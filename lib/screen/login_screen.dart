import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:magang_javan/api/auth_repository.dart';
import 'package:magang_javan/bloc/auth_bloc/auth_bloc.dart';
import 'package:magang_javan/bloc/login_screen/login_screen_bloc.dart';
import 'package:magang_javan/router/route_name.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  LoginScreenBloc loginScreenBloc;

  @override
  void initState() {
    loginScreenBloc =
        LoginScreenBloc(AuthRepository(), context.read<AuthBloc>());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size screen = MediaQuery.of(context).size;
    return BlocConsumer<LoginScreenBloc, LoginScreenState>(
        bloc: loginScreenBloc,
        listener: (context, state) {
          if (state is SuccessLogin) {
            Navigator.pushReplacementNamed(context, HOME);
          }
        },
        builder: (context, state) {
          return LoadingOverlay(
            isLoading: state.isLoading,
            child: GestureDetector(
              onTap: (){
                FocusScope.of(context).unfocus();
              },
              child: Scaffold(
                resizeToAvoidBottomInset: true,
                body: SafeArea(
                  child: SingleChildScrollView(
                    physics: ClampingScrollPhysics(),
                    child: Container(
                      height: screen.height,
                      width: screen.width,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: screen.height * 0.06,
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: screen.width * 0.05),
                            child: Text(
                              'Welcome to Book Rental',
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: screen.width * 0.05,
                                top: screen.height * 0.01,
                                right: screen.width * 0.2),
                            child: Text(
                              'Silahkan Masuk dengan menggunakan E-mail dan Password yang sesuai',
                            ),
                          ),
                          SizedBox(
                            height: screen.height * 0.05,
                          ),
                          Expanded(
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.vertical(
                                    top: Radius.circular(40)),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        vertical: screen.height * 0.02,
                                        horizontal: screen.width * 0.08),
                                    child: Text(
                                      'LOGIN FORM',
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  SizedBox(
                                    height: screen.height * 0.05,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: screen.width * 0.1),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.stretch,
                                      children: [
                                        Text(
                                          'Email',
                                        ),
                                        TextFormField(
                                          controller: emailController,
                                          keyboardType: TextInputType.emailAddress,
                                        ),
                                        SizedBox(
                                          height: screen.height * 0.02,
                                        ),
                                        Text(
                                          'Password',
                                        ),
                                        TextFormField(
                                            controller: passwordController,
                                            obscureText: true,
                                        ),
                                        Container(
                                          padding: EdgeInsets.all(10),
                                          child: Align(
                                              alignment: Alignment.center,
                                              child: Text(
                                                state.validationText,
                                                style:
                                                    TextStyle(color: Colors.red),
                                              )),
                                        ),
                                        SizedBox(
                                          height: screen.height * 0.02,
                                        ),
                                        FlatButton(
                                            onPressed: () {
                                              loginScreenBloc.add(
                                                  SubmitLogin(
                                                      emailController.text.trim(),
                                                      passwordController.text
                                                          .trim()));
                                            },
                                            child: Text("LOGIN")),
                                        SizedBox(
                                          height: screen.height * 0.02,
                                        ),
                                        InkWell(
                                          onTap: () {
                                            Navigator.pushNamed(
                                                context, REGISTER);
                                          },
                                          child: Container(
                                              child: Align(
                                                  alignment: Alignment.center,
                                                  child: Text(
                                                      "Belum memiliki akun?"))),
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        });
  }
}
