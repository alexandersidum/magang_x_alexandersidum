import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:magang_javan/bloc/auth_bloc/auth_bloc.dart';
import 'package:magang_javan/bloc/my_book_screen/my_book_screen_bloc.dart';
import 'package:magang_javan/bloc/peminjam_home_screen/peminjam_home_screen_bloc.dart';
import 'package:magang_javan/model/book.dart';
import 'package:magang_javan/widget/book_tile.dart';
import 'package:flutter/services.dart';
import 'package:magang_javan/widget/book_tile_peminjam.dart';

class MyPinjamanScreen extends StatefulWidget {
  @override
  _MyPinjamanScreenState createState() => _MyPinjamanScreenState();
}

class _MyPinjamanScreenState extends State<MyPinjamanScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    imageCache.clear();
    context
        .read<PeminjamHomeScreenBloc>()
        .add(LoadBook(1));
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<PeminjamHomeScreenBloc, PeminjamHomeScreenState>(
        builder: (context, state) {
      return LoadingOverlay(
        isLoading: state.isLoading,
        child: Stack(children: [
          Container(
            child: ListView(
                children: state.listBook.length > 0
                    ? state.listBook
                        .map((e) => BookTilePeminjam(
                          book : e,
                          onDeskripsi : (){},
                          onPinjam : (){},
                          ))
                        .toList()
                    : [
                        Center(
                          child: Text("Buku tidak ditemukan"),
                        )
                      ]),
          ),
          Positioned(
              bottom: 1,
              right: 1,
              child: IconButton(
                  iconSize: 40,
                  icon: Icon(Icons.refresh),
                  onPressed: () {
                    context.read<PeminjamHomeScreenBloc>().add(
                        LoadBook(context.read<AuthBloc>().state.user.ID));
                  }))
        ]),
      );
    }, listener: (context, state) {
      if (state.message.isNotEmpty) {
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text(state.message)));
        context
            .read<PeminjamHomeScreenBloc>()
            .add(LoadBook(context.read<AuthBloc>().state.user.ID));
      }
    });
  }
}
