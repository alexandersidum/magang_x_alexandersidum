import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:magang_javan/api/book_repository.dart';
import 'package:magang_javan/bloc/auth_bloc/auth_bloc.dart';
import 'package:magang_javan/bloc/home_screen/home_screen_bloc.dart';
import 'package:magang_javan/bloc/my_book_screen/my_book_screen_bloc.dart';
import 'package:magang_javan/bloc/peminjam_home_screen/peminjam_home_screen_bloc.dart';
import 'package:magang_javan/router/route_name.dart';
import 'package:magang_javan/screen/pemilik_screen/my_book_screen.dart';
import 'package:magang_javan/screen/peminjam_screen/peminjam_home_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<HomeScreenBloc, HomeScreenState>(
        listener: (context, state) {
    }, builder: (context, state) {
      return Scaffold(
        appBar: AppBar(title: Text("Book Rental")),
        drawer: context.read<AuthBloc>().state.user.roles_id == 1
            ? _peminjamDrawer()
            : _pemilikDrawer(),
        body:context.read<AuthBloc>().state.user.roles_id == 1
            ? _buildBodyPeminjam(state.selectedScreen)
            : _buildBodyPemilik(state.selectedScreen)
      );
    });
  }

  Widget _pemilikDrawer() {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.all(0.0),
        children: [
          DrawerHeader(
            child: Container(),
            decoration: BoxDecoration(
                color: Colors.blueAccent,
                image: DecorationImage(
                    image: AssetImage("assets/images/blue_book.jpg"),
                    fit: BoxFit.cover)),
          ),
          ListTile(
              dense: true,
              subtitle: Text("Halaman Utama"),
              leading: Icon(Icons.home),
              title: Text("Home"),
              onTap: () {
                context.read<HomeScreenBloc>().add(ChangeScreen(0));
                Navigator.pop(context);
              }),
          Divider(),
          ListTile(
              dense: true,
              subtitle: Text("Koleksi buku"),
              leading: Icon(Icons.book),
              title: Text("Buku Saya"),
              onTap: () {
                context.read<HomeScreenBloc>().add(ChangeScreen(1));
                Navigator.pop(context);
              }),
          Divider(),
          ListTile(
              dense: true,
              subtitle: Text("Keluar dari aplikasi"),
              leading: Icon(Icons.exit_to_app),
              title: Text("Log Out"),
              onTap: () {
                context.read<AuthBloc>().add(LoggedOut());
                Navigator.of(context).pushReplacementNamed(LOGIN);
              }),
          Divider(),
        ],
      ),
    );
  }

  Widget _peminjamDrawer() {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.all(0.0),
        children: [
          DrawerHeader(
            child: Container(),
            decoration: BoxDecoration(
                color: Colors.blueAccent,
                image: DecorationImage(
                    image: AssetImage("assets/images/purple_book.jpg"),
                    fit: BoxFit.cover)),
          ),
          ListTile(
              dense: true,
              subtitle: Text("Halaman Utama"),
              leading: Icon(Icons.home),
              title: Text("Home"),
              onTap: () {
                context.read<HomeScreenBloc>().add(ChangeScreen(0));
                Navigator.pop(context);
              }),
          Divider(),
          ListTile(
              dense: true,
              subtitle: Text("Buku Pinjaman Saya"),
              leading: Icon(Icons.book),
              title: Text("Pinjaman Saya"),
              onTap: () {
                context.read<HomeScreenBloc>().add(ChangeScreen(1));
                Navigator.pop(context);
              }),
          Divider(),
          ListTile(
              dense: true,
              subtitle: Text("Keluar dari aplikasi"),
              leading: Icon(Icons.exit_to_app),
              title: Text("Log Out"),
              onTap: () {
                context.read<AuthBloc>().add(LoggedOut());
                Navigator.of(context).pushReplacementNamed(LOGIN);
              }),
          Divider(),
        ],
      ),
    );
  }

  Widget _buildBodyPemilik(int selectedScreen) {
    switch (selectedScreen) {
      case 0:
        return Center(
          child: Text("Hello World"),
        );
      case 1:
        return BlocProvider(
          create: (_) => MyBookScreenBloc(BookRepository()),
          child: MyBookScreen(),
        );
      case 2:
        return Center(
          child: Text("3"),
        );
      default:
        return Center(
          child: Text("Hello World"),
        );
    }
  }
  Widget _buildBodyPeminjam(int selectedScreen) {
    switch (selectedScreen) {
      case 0:
        return BlocProvider(
          create: (_) => PeminjamHomeScreenBloc(BookRepository()),
          child: PeminjamHomeScreen(),
        );
      case 1:
        return BlocProvider(
          create: (_) => PeminjamHomeScreenBloc(BookRepository()),
          child: Center(
            child: Text("Pinjaman Screen"),
          ),
        );
      case 2:
        return Center(
          child: Text("3"),
        );
      default:
        return Center(
          child: Text("Hello World"),
        );
    }
  }
}
