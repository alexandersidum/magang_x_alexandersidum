import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:magang_javan/bloc/auth_bloc/auth_bloc.dart';
import 'package:magang_javan/bloc/edit_book_screen/edit_book_screen_bloc.dart';
import 'package:magang_javan/model/book.dart';
import 'package:image_picker/image_picker.dart';
import 'package:magang_javan/utils/helper.dart';

class EditBookScreen extends StatefulWidget {
  //Mode 0 Add Mode 1 Edit
  EditBookScreen(this.isEdit);
  bool isEdit;
  @override
  _EditBookScreenState createState() => _EditBookScreenState();
}

class _EditBookScreenState extends State<EditBookScreen> {
  TextEditingController judulBukuController = TextEditingController();
  TextEditingController deskripsiController = TextEditingController();
  TextEditingController hargaController = TextEditingController();
  @override
  void initState() {
    super.initState();
    context.read<EditBookScreenBloc>().add(LoadGenre());
    Book bookState = context.read<EditBookScreenBloc>().state.book;
    if (bookState != null) {
      judulBukuController.text = bookState.judulBuku;
      deskripsiController.text = bookState.deskripsi;
      hargaController.text = bookState.harga.toString();
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.isEdit ? editPage(context) : addPage(context);
  }

  Widget editPage(BuildContext context) {
    return BlocConsumer<EditBookScreenBloc, EditBookScreenState>(
      listener: (context, state) {
        print("mesage ${state.message}");
        // if (state.book != null) {
        //   if(judulBukuController.text?.isEmpty??true||deskripsiController.text?.isEmpty??true){
        //     judulBukuController.text = state.book.judulBuku;
        //     deskripsiController.text = state.book.deskripsi;
        //     hargaController.text = state.book.harga.toString();
        //   }
        // }
        if (state.message.isNotEmpty) {
          print("Message is not empty");
          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(content: Text(state.message)));
        }
      },
      builder: (context, state) {
        return Scaffold(
          appBar: appBar(state),
          body: buildBodyEdit(state),
        );
      },
    );
  }

  Widget addPage(BuildContext context) {
    return BlocConsumer<EditBookScreenBloc, EditBookScreenState>(
      listener: (context, state) {
        if (state.message.isNotEmpty) {
          print("Message is not empty");
          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(content: Text(state.message)));
        }
      },
      builder: (context, state) {
        return Scaffold(
          appBar: appBarAdd(state),
          body: buildBodyAdd(state),
        );
      },
    );
  }

  Widget appBar(EditBookScreenState) {
    return AppBar(
      title: Text("Edit Book"),
      centerTitle: true,
    );
  }

  Widget appBarAdd(EditBookScreenState) {
    return AppBar(
      title: Text("Add Book"),
      centerTitle: true,
    );
  }

  Widget buildBodyEdit(EditBookScreenState state) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    height: kScreenWidth(context) / 2,
                    width: kScreenWidth(context) / 2,
                    child: Image(
                      image: state.pickedimage == null
                          ? NetworkImage(state.book.gambar)
                          : FileImage(state.pickedimage),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                FlatButton(
                    onPressed: () async {
                      PickedFile pickedImage = await ImagePicker()
                          .getImage(source: ImageSource.gallery);
                      if (pickedImage != null) {
                        context
                            .read<EditBookScreenBloc>()
                            .add(AddPhoto(File(pickedImage.path)));
                      }
                    },
                    child: Text("Add image"))
              ],
            ),
            TextFormField(
              decoration: InputDecoration(
                  labelText: "JudulBuku",
                  enabledBorder:
                      OutlineInputBorder(borderSide: BorderSide(width: 1))),
              controller: judulBukuController,
              keyboardType: TextInputType.text,
            ),
            SizedBox(height: 20),
            TextFormField(
              decoration: InputDecoration(
                  labelText: "Deskripsi",
                  enabledBorder:
                      OutlineInputBorder(borderSide: BorderSide(width: 1))),
              controller: deskripsiController,
              keyboardType: TextInputType.text,
            ),
            SizedBox(height: 20),
            Text(
              'Genre Buku',
            ),
            DropdownButtonHideUnderline(
                child: DropdownButton(
                    onChanged: (value) {
                      context
                          .read<EditBookScreenBloc>()
                          .add(EditInfo(state.book.copyWith(genreId: value)));
                    },
                    value: state.book.genreId,
                    items: state.listGenre
                        .map((e) => DropdownMenuItem(
                              child: Text(e.genreName),
                              value: e.id,
                            ))
                        .toList())),
            SizedBox(height: 20),
            TextFormField(
              decoration: InputDecoration(
                  labelText: "Harga",
                  enabledBorder:
                      OutlineInputBorder(borderSide: BorderSide(width: 1))),
              controller: hargaController,
              keyboardType: TextInputType.text,
            ),
            CheckboxListTile(
              title: Text('Is Published'),
              value: state.book.isPublish,
              onChanged: (bool value) {
                setState(() {
                  context
                      .read<EditBookScreenBloc>()
                      .add(EditInfo(state.book.copyWith(isPublish: value)));
                });
              },
            ),
            Align(
                alignment: Alignment.center,
                child: FlatButton(
                  minWidth: kScreenWidth(context),
                  height: kScreenHeight(context) / 15,
                  color: Colors.blueAccent,
                  onPressed: () {
                    context.read<EditBookScreenBloc>().add(SubmitEdit(
                        state.book.copyWith(
                          deskripsi: deskripsiController.text.trim(),
                          harga: int.parse(hargaController.text.trim()),
                          judulBuku: judulBukuController.text.trim(),
                        ),
                        state.pickedimage));
                  },
                  child: Text("Submit Book",
                      style: TextStyle(
                        color: Colors.white,
                      )),
                ))
          ],
        ),
      ),
    );
  }

  Widget buildBodyAdd(EditBookScreenState state) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    color: Colors.grey[400],
                    height: kScreenWidth(context) / 2,
                    width: kScreenWidth(context) / 2,
                    child: state.pickedimage == null
                        ? Container()
                        : Image(
                            image: FileImage(state.pickedimage),
                            fit: BoxFit.cover,
                          ),
                  ),
                ),
                FlatButton(
                    onPressed: () async {
                      PickedFile pickedImage = await ImagePicker()
                          .getImage(source: ImageSource.gallery);
                      if (pickedImage != null) {
                        context
                            .read<EditBookScreenBloc>()
                            .add(AddPhoto(File(pickedImage.path)));
                      }
                    },
                    child: Text("Edit image"))
              ],
            ),
            TextFormField(
              decoration: InputDecoration(
                  labelText: "JudulBuku",
                  enabledBorder:
                      OutlineInputBorder(borderSide: BorderSide(width: 1))),
              controller: judulBukuController,
              keyboardType: TextInputType.text,
            ),
            SizedBox(height: 20),
            TextFormField(
              decoration: InputDecoration(
                  labelText: "Deskripsi",
                  enabledBorder:
                      OutlineInputBorder(borderSide: BorderSide(width: 1))),
              controller: deskripsiController,
              keyboardType: TextInputType.text,
            ),
            SizedBox(height: 20),
            Text(
              'Genre Buku',
            ),
            DropdownButtonHideUnderline(
                child: DropdownButton(
                    onChanged: (value) {
                      context.read<EditBookScreenBloc>().add(EditInfo(
                          state.book != null
                              ? state.book.copyWith(genreId: value)
                              : Book(genreId: value)));
                    },
                    value: state.book != null ? state.book.genreId : null,
                    items: state.listGenre
                        .map((e) => DropdownMenuItem(
                              child: Text(e.genreName),
                              value: e.id,
                            ))
                        .toList())),
            SizedBox(height: 20),
            TextFormField(
              decoration: InputDecoration(
                  labelText: "Harga",
                  enabledBorder:
                      OutlineInputBorder(borderSide: BorderSide(width: 1))),
              controller: hargaController,
              keyboardType: TextInputType.text,
            ),
            CheckboxListTile(
              title: Text('Is Published'),
              value: state.book != null ? state.book.isPublish??false : false,
              onChanged: (bool value) {
                setState(() {
                  context.read<EditBookScreenBloc>().add(EditInfo(
                      state.book != null
                          ? state.book.copyWith(isPublish: value)
                          : Book(isPublish: value)));
                });
              },
            ),
            Align(
                alignment: Alignment.center,
                child: FlatButton(
                  minWidth: kScreenWidth(context),
                  height: kScreenHeight(context) / 15,
                  color: Colors.blueAccent,
                  onPressed: () {
                    context.read<EditBookScreenBloc>().add(SubmitBook(
                      state.book!=null?
                        state.book.copyWith(
                          deskripsi: deskripsiController.text.trim(),
                          harga: int.parse(hargaController.text.trim()),
                          judulBuku: judulBukuController.text.trim(),
                          pemilikId: context.read<AuthBloc>().state.user.ID
                        ):Book(
                          deskripsi: deskripsiController.text.trim(),
                          harga: int.parse(hargaController.text.trim()),
                          judulBuku: judulBukuController.text.trim(),
                          pemilikId: context.read<AuthBloc>().state.user.ID
                        ),
                        state.pickedimage));
                  },
                  child: Text("Submit Book",
                      style: TextStyle(
                        color: Colors.white,
                      )),
                ))
          ],
        ),
      ),
    );
  }
}
