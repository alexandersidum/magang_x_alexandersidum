import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:magang_javan/api/book_repository.dart';
import 'package:magang_javan/bloc/auth_bloc/auth_bloc.dart';
import 'package:magang_javan/bloc/edit_book_screen/edit_book_screen_bloc.dart';
import 'package:magang_javan/bloc/my_book_screen/my_book_screen_bloc.dart';
import 'package:magang_javan/model/book.dart';
import 'package:magang_javan/screen/pemilik_screen/edit_book_screen.dart';
import 'package:magang_javan/widget/book_tile.dart';
import 'package:flutter/services.dart';

class MyBookScreen extends StatefulWidget {
  @override
  _MyBookScreenState createState() => _MyBookScreenState();
}

class _MyBookScreenState extends State<MyBookScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    imageCache.clear();
    context
        .read<MyBookScreenBloc>()
        .add(LoadMyBook(context.read<AuthBloc>().state.user.ID));
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<MyBookScreenBloc, MyBookScreenState>(
        builder: (context, state) {
      return LoadingOverlay(
        isLoading: state.isLoading,
        child: Stack(children: [
          Container(
            child: ListView(
                children: state.listBook.length > 0
                    ? state.listBook
                        .map((e) => BookTile(e, () {
                              context
                                  .read<MyBookScreenBloc>()
                                  .add(DeleteBook(e.id));
                            }))
                        .toList()
                    : [
                        Center(
                          child: Text("Buku tidak ditemukan"),
                        )
                      ]),
          ),
          Positioned(
              bottom: 1,
              right: 1,
              child: IconButton(
                  iconSize: 40,
                  icon: Icon(Icons.refresh),
                  onPressed: () {
                    context.read<MyBookScreenBloc>().add(
                        LoadMyBook(context.read<AuthBloc>().state.user.ID));
                  })),
          Positioned(
              bottom: 1,
              left: 1,
              child: IconButton(
                  iconSize: 50,
                  icon: Icon(Icons.add),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => BlocProvider(
                            create: (context) =>
                                EditBookScreenBloc(BookRepository(), null),
                            child: EditBookScreen(false))));
                  }))
        ]),
      );
    }, listener: (context, state) {
      if (state.message.isNotEmpty) {
        ScaffoldMessenger.of(context)
            .showSnackBar(SnackBar(content: Text(state.message)));
        context
            .read<MyBookScreenBloc>()
            .add(LoadMyBook(context.read<AuthBloc>().state.user.ID));
      }
    });
  }
}
