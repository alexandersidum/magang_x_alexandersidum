import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:magang_javan/bloc/register_screen/register_screen_bloc.dart';
import 'package:magang_javan/router/route_name.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController namaController = TextEditingController();
  TextEditingController teleponController = TextEditingController();
  TextEditingController alamatontroller = TextEditingController();
  int role;

  @override
  Widget build(BuildContext context) {
    Size screen = MediaQuery.of(context).size;
    return BlocConsumer<RegisterScreenBloc, RegisterScreenState>(
        listener: (context, state) {
      if (state is SuccessRegister) {
        ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(content: Text("Registrasi Sukses, Silahkan Login")));
        Navigator.pop(context);
      }
    }, builder: (context, state) {
      return LoadingOverlay(
        isLoading: state.isLoading,
        child: Scaffold(
          resizeToAvoidBottomInset: true,
          body: Container(
            child: SingleChildScrollView(
              child: Container(
                height: screen.height * 1.5,
                width: screen.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: screen.height * 0.06,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: screen.width * 0.05),
                      child: Text(
                        'Welcome to Register ',
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          left: screen.width * 0.05,
                          top: screen.height * 0.01,
                          right: screen.width * 0.2),
                      child: Text(
                        'Silahkan Masukkan Data yang Diperlukan',
                      ),
                    ),
                    SizedBox(
                      height: screen.height * 0.05,
                    ),
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.vertical(top: Radius.circular(40)),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: [
                            SizedBox(
                              height: screen.height * 0.05,
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: screen.width * 0.1),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Text(
                                    'Nama',
                                  ),
                                  TextFormField(
                                    controller: namaController,
                                  ),
                                  SizedBox(
                                    height: screen.height * 0.02,
                                  ),
                                  Text(
                                    'Email',
                                  ),
                                  TextFormField(
                                    controller: emailController,
                                  ),
                                  SizedBox(
                                    height: screen.height * 0.02,
                                  ),
                                  Text(
                                    'No. Telepon',
                                  ),
                                  TextFormField(
                                    controller: teleponController,
                                  ),
                                  SizedBox(
                                    height: screen.height * 0.02,
                                  ),
                                  Text(
                                    'Alamat',
                                  ),
                                  TextFormField(
                                    controller: alamatontroller,
                                  ),
                                  SizedBox(
                                    height: screen.height * 0.02,
                                  ),
                                  Text(
                                    'Role',
                                  ),
                                  DropdownButtonHideUnderline(
                                    child: DropdownButton(
                                        hint: Text("Pilih role"),
                                        value: role,
                                        items: [
                                          DropdownMenuItem<int>(
                                              value: 1,
                                              child: Text("Peminjam")),
                                          DropdownMenuItem<int>(
                                              value: 2, child: Text("Pemilik"))
                                        ],
                                        onChanged: (value) {
                                          setState(() {
                                            role = value;
                                          });
                                        }),
                                  ),
                                  SizedBox(
                                    height: screen.height * 0.02,
                                  ),
                                  Text(
                                    'Password',
                                  ),
                                  TextFormField(
                                    controller: passwordController,
                                    obscureText: true,
                                  ),
                                  SizedBox(
                                    height: screen.height * 0.06,
                                  ),
                                  SizedBox(
                                    height: screen.height * 0.02,
                                  ),
                                  FlatButton(
                                      onPressed: () {
                                        context
                                            .read<RegisterScreenBloc>()
                                            .add(SubmitRegister(
                                              emailController.text,
                                              passwordController.text,
                                              namaController.text,
                                              alamatontroller.text,
                                              role,
                                              teleponController.text,
                                            ));
                                      },
                                      child: Text("REGISTER"))
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    });
  }
}
