
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:magang_javan/api/auth_repository.dart';
import 'package:magang_javan/bloc/auth_bloc/auth_bloc.dart';
import 'package:magang_javan/bloc/home_screen/home_screen_bloc.dart';
import 'package:magang_javan/bloc/login_screen/login_screen_bloc.dart';
import 'package:magang_javan/bloc/register_screen/register_screen_bloc.dart';
import 'package:magang_javan/router/route_name.dart';
import 'package:magang_javan/screen/home_screen.dart';
import 'package:magang_javan/screen/login_screen.dart';
import 'package:magang_javan/screen/register_screen.dart';

Route<dynamic> generateRoute(RouteSettings setting){
  print("generateroute terpanggel");
  switch (setting.name) {
    case HOME:
      print("ROUTE KE HOME");
      return MaterialPageRoute(builder: (_)=>BlocProvider(
        create: (_)=>HomeScreenBloc(AuthRepository()),
        child: HomeScreen()));
    case LOGIN:
      print("ROUTE KE LOGIN");
      return MaterialPageRoute(builder: (_)=>LoginScreen());
    case REGISTER:
      print("ROUTE KE REGISTER");
      return MaterialPageRoute(builder: (_)=>BlocProvider(
        create: (_)=>RegisterScreenBloc(AuthRepository()),
        child: RegisterScreen()));
    default:
      return MaterialPageRoute(builder: (_)=>Scaffold(
        body: Center(
          child: Text("Wrong Route"),
        ),
      ));
  }

}

