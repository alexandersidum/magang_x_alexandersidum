import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:magang_javan/api/book_repository.dart';
import 'package:magang_javan/bloc/edit_book_screen/edit_book_screen_bloc.dart';
import 'package:magang_javan/model/book.dart';
import 'package:magang_javan/screen/pemilik_screen/edit_book_screen.dart';
import 'package:magang_javan/utils/helper.dart';

class BookTile extends StatelessWidget {
  final Book book;
  final Function onDelete;
  BookTile(this.book , this.onDelete);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: Colors.grey[100],
        ),
        padding: EdgeInsets.all(1),
        height: kisPortrait(context)
            ? kScreenHeight(context) / 6
            : kScreenHeight(context) / 4,
        child: Row(children: [
          Expanded(
            flex: 1,
            child: Container(
                height: double.infinity,
                padding: EdgeInsets.all(10),
                child: Image(
                  image: NetworkImage(book.gambar),
                  fit: BoxFit.fill,
                )),
          ),
          Expanded(
              flex: 2,
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Column(
                  children: [
                    Expanded(
                        child: Text(
                      book.judulBuku,
                      style: TextStyle(fontWeight: FontWeight.w600),
                    )),
                    Expanded(
                        child: Text(
                      book.isPublish ? "Published" : "Not Published",
                      style: TextStyle(
                          color: book.isPublish
                              ? Colors.green[600]
                              : Colors.blueGrey),
                    )),
                    Expanded(
                      child: Row(
                        children: [
                          Expanded(
                              child: FlatButton(
                                  color: Colors.blueAccent,
                                  onPressed: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) => BlocProvider(
                                                create: (context) =>
                                                    EditBookScreenBloc(
                                                        BookRepository(), book),
                                                child: EditBookScreen(true))));
                                  },
                                  child: Text(
                                    "Edit",
                                    style: TextStyle(color: Colors.white),
                                  ))),
                          SizedBox(
                            width: 10,
                          ),
                          Expanded(
                              child: FlatButton(
                                  color: Colors.redAccent,
                                  onPressed: () {
                                    onDelete();
                                  },
                                  child: Text(
                                    "Hapus",
                                    style: TextStyle(color: Colors.white),
                                  ))),
                        ],
                      ),
                    )
                  ],
                ),
              )),
        ]));
  }

   
}

