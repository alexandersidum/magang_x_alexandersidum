part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}

class LoggedIn extends AuthEvent{
  final User user;

  LoggedIn(this.user); 
}

class LoggedOut extends AuthEvent{

}
