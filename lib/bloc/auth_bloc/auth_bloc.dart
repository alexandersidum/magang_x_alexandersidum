import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:magang_javan/bloc/my_book_screen/my_book_screen_bloc.dart';
import 'package:magang_javan/model/user.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  AuthBloc() : super(AuthState(null));

  @override
  Stream<AuthState> mapEventToState(
    AuthEvent event,
  ) async* {
    if (event is LoggedIn) {
      print(event.user);
      yield state.copyWith(user: event.user);
    }
    if (event is LoggedOut) {
      yield state.copyWith(user: null);
    }
  }
}
