part of 'add_book_screen_bloc.dart';

abstract class AddBookScreenEvent extends Equatable {
  const AddBookScreenEvent();

  @override
  List<Object> get props => [];
}
