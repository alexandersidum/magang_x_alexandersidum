import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:magang_javan/model/book.dart';
import 'dart:io';

part 'add_book_screen_event.dart';
part 'add_book_screen_state.dart';

class AddBookScreenBloc extends Bloc<AddBookScreenEvent, AddBookScreenState> {
  AddBookScreenBloc() : super(AddBookScreenState());

  @override
  Stream<AddBookScreenState> mapEventToState(
    AddBookScreenEvent event,
  ) async* {
    // TODO: implement mapEventToState
  }
}
