part of 'add_book_screen_bloc.dart';

class AddBookScreenState extends Equatable {
  AddBookScreenState({this.book, this.pickedimage});
  final Book book;
  File pickedimage;
  @override
  List<Object> get props => [book, pickedimage];

  AddBookScreenState copyWith({
    Book book,
    File pickedimage
  }) {
    return AddBookScreenState(
      book:book ?? this.book,
      pickedimage : pickedimage ?? this.pickedimage
    );
  }
}
class AddBookFailure extends AddBookScreenState{
  final String message;

  AddBookFailure(this.message);
  @override
  List<Object> get props => [message];
}

class AddBookSuccess extends AddBookScreenState{
}