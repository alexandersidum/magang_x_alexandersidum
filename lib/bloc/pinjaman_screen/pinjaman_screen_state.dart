part of 'pinjaman_screen_bloc.dart';

abstract class PinjamanScreenState extends Equatable {
  const PinjamanScreenState();
  
  @override
  List<Object> get props => [];
}

class PinjamanScreenInitial extends PinjamanScreenState {}
