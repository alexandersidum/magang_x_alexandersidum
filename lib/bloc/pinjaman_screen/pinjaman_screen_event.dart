part of 'pinjaman_screen_bloc.dart';

abstract class PinjamanScreenEvent extends Equatable {
  const PinjamanScreenEvent();

  @override
  List<Object> get props => [];
}
