import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'pinjaman_screen_event.dart';
part 'pinjaman_screen_state.dart';

class PinjamanScreenBloc extends Bloc<PinjamanScreenEvent, PinjamanScreenState> {
  PinjamanScreenBloc() : super(PinjamanScreenInitial());

  @override
  Stream<PinjamanScreenState> mapEventToState(
    PinjamanScreenEvent event,
  ) async* {
    // TODO: implement mapEventToState
  }
}
