import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:magang_javan/api/book_repository.dart';
import 'package:magang_javan/model/book.dart';

part 'edit_book_screen_event.dart';
part 'edit_book_screen_state.dart';

class EditBookScreenBloc
    extends Bloc<EditBookScreenEvent, EditBookScreenState> {
  EditBookScreenBloc(this._bookRepository, Book initialBook)
      : super(EditBookScreenState(book: initialBook));
  final BookRepository _bookRepository;

  @override
  Stream<EditBookScreenState> mapEventToState(
    EditBookScreenEvent event,
  ) async* {
    if (event is AddPhoto) {
      yield state.copyWith(pickedimage: event.pickedImage);
    } else if (event is EditInfo) {
      yield state.copyWith(book: event.book);
    } else if (event is SubmitEdit) {
      print("is on bloc submit");
      yield* _submitEdit(book: event.book, pickedFile: event.pickedimage);
    } else if (event is SubmitBook) {
      yield* _submitBook(book: event.book, pickedFile: event.pickedimage);
    } else if (event is LoadGenre) {
      yield* _loadGenre();
    }
  }

  Stream<EditBookScreenState> _submitEdit({Book book, File pickedFile}) async* {
    print("is on bloc submitedit");
    yield state.copyWith(isLoading: true);
    try {
      bool isSuccess = await _bookRepository.editBook(book, pickedFile);
      if (isSuccess) {
        print("is on bloc submit success");
        yield state.copyWith(message: "Success");
      } else {
        print("is on bloc submit else ");
        yield state.copyWith(message: "Failure");
      }
    } on Exception catch (e) {
      print("is on catchedd submit $e");
      yield state.copyWith(message: e.toString());
    }
  }

  Stream<EditBookScreenState> _submitBook({Book book, File pickedFile}) async* {
    print("is on bloc submitedit");
    yield state.copyWith(isLoading: true);
    try {
      bool isSuccess = await _bookRepository.postBook(book, pickedFile);
      if (isSuccess) {
        print("is on bloc submit success");
        yield state.copyWith(message: "Success");
      } else {
        print("is on bloc submit else ");
        yield state.copyWith(message: "Failure");
      }
    } on Exception catch (e) {
      print("is on catchedd submit $e");
      yield state.copyWith(message: e.toString());
    }
  }

  Stream<EditBookScreenState> _loadGenre() async* {
    yield state.copyWith(isLoading: true);
    try {
      List<Genre> listGenre = await _bookRepository.getGenre();
      yield state.copyWith(isLoading: false, listGenre: listGenre);
    } on Exception catch (e) {
      yield state.copyWith(isLoading: false);
      yield state.copyWith(message: e.toString());
    }
  }
}
