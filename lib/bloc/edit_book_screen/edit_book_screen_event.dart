part of 'edit_book_screen_bloc.dart';

abstract class EditBookScreenEvent extends Equatable {
  const EditBookScreenEvent();

  @override
  List<Object> get props => [];
}

class AddPhoto extends EditBookScreenEvent{
  final File pickedImage;

  AddPhoto(this.pickedImage);
  @override
  List<Object> get props => [pickedImage];
}

class EditInfo extends EditBookScreenEvent{
  final Book book;

  EditInfo(this.book);
  @override
  List<Object> get props => [book];
}

class LoadGenre extends EditBookScreenEvent {}

class SubmitEdit extends EditBookScreenEvent{
  final Book book;
  final File pickedimage;

  SubmitEdit(this.book, this.pickedimage);
}

class SubmitBook extends EditBookScreenEvent{
  final Book book;
  final File pickedimage;

  SubmitBook(this.book, this.pickedimage);
}
