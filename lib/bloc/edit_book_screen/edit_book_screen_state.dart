part of 'edit_book_screen_bloc.dart';

class EditBookScreenState extends Equatable {
  EditBookScreenState(
      {this.book, this.pickedimage, this.message ='', this.isLoading = false, this.listGenre =const[]});
  final Book book;
  File pickedimage;
  String message;
  bool isLoading = false;
  List<Genre> listGenre = [];
  @override
  List<Object> get props => [book, pickedimage, isLoading,listGenre, message];

  EditBookScreenState copyWith(
      {Book book, File pickedimage, String message, bool isLoading, List<Genre> listGenre}) {
    return EditBookScreenState(
        book: book ?? this.book,
        pickedimage: pickedimage ?? this.pickedimage,
        isLoading: isLoading ?? this.isLoading,
        listGenre: listGenre ?? this.listGenre,
        message: message ?? this.message
        );
  }
}


