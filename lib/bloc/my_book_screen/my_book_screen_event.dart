part of 'my_book_screen_bloc.dart';

abstract class MyBookScreenEvent extends Equatable {
  const MyBookScreenEvent();

  @override
  List<Object> get props => [];
}

class LoadMyBook extends MyBookScreenEvent{
  final int userId;
  LoadMyBook(this.userId);
  @override
  List<Object> get props => [userId];
}

class DeleteBook extends MyBookScreenEvent{
  final int bookId;
  DeleteBook(this.bookId);
  @override
  List<Object> get props => [bookId];
}

