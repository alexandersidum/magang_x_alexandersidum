import 'dart:async';
import 'package:magang_javan/model/book.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:magang_javan/api/book_repository.dart';

part 'my_book_screen_event.dart';
part 'my_book_screen_state.dart';

class MyBookScreenBloc extends Bloc<MyBookScreenEvent, MyBookScreenState> {
  MyBookScreenBloc(this._bookRepository) : super(MyBookScreenState());
  final BookRepository _bookRepository;

  @override
  Stream<MyBookScreenState> mapEventToState(
    MyBookScreenEvent event,
  ) async* {
    if(event is LoadMyBook){
      yield* _loadBook(event.userId);
    }
    if(event is DeleteBook){
      yield* _deleteBook(event.bookId);
    }
  }

  Stream<MyBookScreenState>_loadBook(int userId)async*{
    yield state.copyWith(
      isLoading: true,
    );
    try{
      print("Loading book");
      List<Book> listBook = await _bookRepository.loadBookByPemilikId(userId);
      print("abisl ist book");
      yield state.copyWith(isLoading: false, listBook: listBook);
    }
    catch(e){
      print("CATCHED Loading book $e");
      yield state.copyWith(isLoading: false);
    }
  }

  Stream<MyBookScreenState>_deleteBook(int bookId)async*{
    yield state.copyWith(
      isLoading: true,
    );
    try{
      bool isSuccess = await _bookRepository.deleteBook(bookId);
      if(isSuccess){
        yield state.copyWith(message: "Success" , isLoading: false);
      }
      else{
        yield state.copyWith(message: "Failed", isLoading: false);
      }
    }
    catch(e){
      yield state.copyWith(message: e.toString(), isLoading: false);
    }
  }
}
