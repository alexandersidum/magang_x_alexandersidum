part of 'my_book_screen_bloc.dart';

class MyBookScreenState extends Equatable {
  const MyBookScreenState({this.listBook=const [], this.isLoading=false , this.message =''});
  final List<Book> listBook;
  final String message;
  final bool isLoading;
  @override
  List<Object> get props => [listBook, isLoading, message];

  MyBookScreenState copyWith({
    List<Book> listBook,
    bool isLoading,
    String message
  }) {
    return MyBookScreenState(
      listBook: listBook ?? this.listBook,
      isLoading: isLoading ?? this.isLoading,
      message: message?? this.message
    );
  }
}

class MyBookScreenInitial extends MyBookScreenState {}

class BookDeletedSuccess extends MyBookScreenState {}
class BookDeletedFailure extends MyBookScreenState {
  final String message;
  BookDeletedFailure(this.message);
  @override
  List<Object> get props => [message];
}
