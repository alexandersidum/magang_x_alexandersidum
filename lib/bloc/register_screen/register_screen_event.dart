part of 'register_screen_bloc.dart';

abstract class RegisterScreenEvent extends Equatable {
  const RegisterScreenEvent();

  @override
  List<Object> get props => [];
}

class SubmitRegister extends RegisterScreenEvent{
  final String email;
  final String password;
  final String nama;
  final String alamat;
  final int role;
  final String telepon; 

  SubmitRegister(this.email, this.password, this.nama, this.alamat, this.role, this.telepon);

  @override
  List<Object> get props => [this.email, this.password, this.nama, this.alamat, this.role, this.telepon];
}
