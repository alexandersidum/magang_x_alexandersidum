import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:magang_javan/api/auth_repository.dart';

part 'register_screen_event.dart';
part 'register_screen_state.dart';

class RegisterScreenBloc
    extends Bloc<RegisterScreenEvent, RegisterScreenState> {
  RegisterScreenBloc(this._authRepository) : super(RegisterScreenState());
  final AuthRepository _authRepository;

  @override
  Stream<RegisterScreenState> mapEventToState(
    RegisterScreenEvent event,
  ) async* {
    if (event is SubmitRegister) {
      yield* _register(event);
    }
  }

  Stream<RegisterScreenState> _register(
    SubmitRegister event,
  ) async* {
    try {
      yield state.copyWith(isLoading: true);
      bool isSuccess = await _authRepository.register(
          alamat: event.alamat,
          email: event.email,
          nama: event.nama,
          password: event.password,
          role: event.role,
          telepon: event.telepon);
      if(isSuccess){
        yield SuccessRegister("Registrasi Berhasil");
      }
      else{
        yield state.copyWith(isLoading: false, validText: "Registrasi Gagal");
      }
    } catch (e) {
      yield state.copyWith(validText: e.toString().replaceAll("Exception: ", ""), isLoading: false);
    }
  }
}
