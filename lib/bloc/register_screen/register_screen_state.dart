part of 'register_screen_bloc.dart';

class RegisterScreenState extends Equatable {
  final bool isLoading;
  final String validText;
  RegisterScreenState({this.isLoading = false, this.validText = ''});

  @override
  List<Object> get props => [isLoading, validText];

  RegisterScreenState copyWith({bool isLoading, String validText}) {
    return RegisterScreenState(
        isLoading: isLoading ?? this.isLoading,
        validText: validText ?? this.validText);
  }
}

class SuccessRegister extends RegisterScreenState {
  SuccessRegister(this.successText):super(isLoading: false,validText: successText);
  String successText ='';
  @override
  List<Object> get props => [successText];
}
