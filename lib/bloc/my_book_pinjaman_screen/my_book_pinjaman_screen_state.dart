part of 'my_book_pinjaman_screen_bloc.dart';

abstract class MyBookPinjamanScreenState extends Equatable {
  const MyBookPinjamanScreenState();
  
  @override
  List<Object> get props => [];
}

class MyBookPinjamanScreenInitial extends MyBookPinjamanScreenState {}
