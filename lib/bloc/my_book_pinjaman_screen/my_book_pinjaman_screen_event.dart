part of 'my_book_pinjaman_screen_bloc.dart';

abstract class MyBookPinjamanScreenEvent extends Equatable {
  const MyBookPinjamanScreenEvent();

  @override
  List<Object> get props => [];
}
