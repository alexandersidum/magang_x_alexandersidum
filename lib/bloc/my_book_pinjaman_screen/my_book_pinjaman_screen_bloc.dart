import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'my_book_pinjaman_screen_event.dart';
part 'my_book_pinjaman_screen_state.dart';

class MyBookPinjamanScreenBloc extends Bloc<MyBookPinjamanScreenEvent, MyBookPinjamanScreenState> {
  MyBookPinjamanScreenBloc() : super(MyBookPinjamanScreenInitial());

  @override
  Stream<MyBookPinjamanScreenState> mapEventToState(
    MyBookPinjamanScreenEvent event,
  ) async* {
    // TODO: implement mapEventToState
  }
}
