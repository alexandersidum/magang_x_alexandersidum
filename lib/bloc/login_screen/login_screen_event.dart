part of 'login_screen_bloc.dart';

@immutable
abstract class LoginScreenEvent extends Equatable{}

class ChangeEmail extends LoginScreenEvent{
  final String email;

  ChangeEmail(this.email);

  @override
  List<Object> get props => [email];
}
class ChangePassword extends LoginScreenEvent{
  final String password;

  ChangePassword(this.password);

  @override
  List<Object> get props => [password];
}

class SubmitLogin extends LoginScreenEvent{
  final String email;
  final String password;

  SubmitLogin(this.email, this.password);

  @override
  List<Object> get props => [email,password];
}
