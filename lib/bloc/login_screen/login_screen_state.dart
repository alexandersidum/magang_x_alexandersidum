part of 'login_screen_bloc.dart';

class LoginScreenState extends Equatable {
  final String email;
  final String password;
  final bool isLoading;
  final String validationText;

  LoginScreenState(
      {this.email = '',
      this.password = '',
      this.isLoading = false,
      this.validationText = ''});

  LoginScreenState copyWith(
      {String email, String password, bool isLoading, String validationText}) {
    return LoginScreenState(
        email: email ?? this.email,
        isLoading: isLoading ?? this.isLoading,
        password: password ?? this.password,
        validationText: validationText ?? this.validationText);
  }

  @override
  List<Object> get props => [email, password, isLoading, validationText];
}

class SuccessLogin extends LoginScreenState {}
