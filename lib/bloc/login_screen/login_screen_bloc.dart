import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:magang_javan/api/auth_repository.dart';
import 'package:magang_javan/bloc/auth_bloc/auth_bloc.dart';
import 'package:magang_javan/model/user.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
part 'login_screen_event.dart';
part 'login_screen_state.dart';

class LoginScreenBloc extends Bloc<LoginScreenEvent, LoginScreenState> {
  LoginScreenBloc(this._authRepository, this._authBloc) : super(LoginScreenState());
  final AuthRepository _authRepository;
  final AuthBloc _authBloc;

  @override
  Stream<LoginScreenState> mapEventToState(
    LoginScreenEvent event,
  ) async* {
    if(event is SubmitLogin){
      yield* _login(event);
    }
    else if(event is ChangeEmail){
      yield state.copyWith(email: event.email);
    }
    else if(event is ChangePassword){
      yield state.copyWith(password: event.password);
    }
  }

  Stream <LoginScreenState> _login(SubmitLogin event)async*{
    yield state.copyWith(isLoading: true);
    try{
      User user = await _authRepository.login(event.email, event.password);
      _authBloc.add(LoggedIn(user));
      yield SuccessLogin();
    }
    on Exception catch(e){
      print("catched in bloc");
      yield state.copyWith(validationText: e.toString().replaceAll("Exception: ", ""), isLoading: false);
    }

  }

}
