part of 'home_screen_bloc.dart';

class HomeScreenState extends Equatable {
  const HomeScreenState({this.selectedScreen = 0});
  final int selectedScreen;
  
  @override
  List<Object> get props => [selectedScreen];

  HomeScreenState copyWith({
    int selectedScreen,
  }) {
    return HomeScreenState(
      selectedScreen: selectedScreen ?? this.selectedScreen,
    );
  }
}

class HomeScreenInitial extends HomeScreenState {}
