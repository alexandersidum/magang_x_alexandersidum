part of 'home_screen_bloc.dart';

abstract class HomeScreenEvent extends Equatable {
  const HomeScreenEvent();

  @override
  List<Object> get props => [];
}

class ChangeScreen extends HomeScreenEvent{
  final int selectedScreen;
  ChangeScreen(this.selectedScreen);

  @override
  List<Object> get props => [selectedScreen];

}
