import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:magang_javan/api/auth_repository.dart';

part 'home_screen_event.dart';
part 'home_screen_state.dart';

class HomeScreenBloc extends Bloc<HomeScreenEvent, HomeScreenState> {
  HomeScreenBloc(this._authRepository) : super(HomeScreenState());
  final AuthRepository _authRepository;
  @override
  Stream<HomeScreenState> mapEventToState(
    HomeScreenEvent event,
  ) async* {
    if(event is ChangeScreen){
      yield state.copyWith(selectedScreen: event.selectedScreen);
    }
  }
}
