part of 'peminjam_home_screen_bloc.dart';

 class PeminjamHomeScreenState extends Equatable {
  final List<Book> listBook;
  final String message;
  final bool isLoading;
  PeminjamHomeScreenState({this.listBook = const[], this.message='', this.isLoading=false});
  @override
  List<Object> get props => [listBook, isLoading, message];

  PeminjamHomeScreenState copyWith({
    List<Book> listBook,
    bool isLoading,
    String message
  }) {
    return PeminjamHomeScreenState(
      listBook: listBook ?? this.listBook,
      isLoading: isLoading ?? this.isLoading,
      message: message?? this.message
    );
  }
}

