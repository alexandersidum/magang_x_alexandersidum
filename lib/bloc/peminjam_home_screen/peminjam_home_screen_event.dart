part of 'peminjam_home_screen_bloc.dart';

abstract class PeminjamHomeScreenEvent extends Equatable {
  const PeminjamHomeScreenEvent();

  @override
  List<Object> get props => [];
}

class LoadBook extends PeminjamHomeScreenEvent{
  final int isPublish;

  LoadBook(this.isPublish);
  @override
  List<Object> get props => [isPublish];
}

class RentBook extends PeminjamHomeScreenEvent{
  final Book book;
  final int userId;

  RentBook( this.book, this.userId);
  @override
  List<Object> get props => [book, userId];
}


