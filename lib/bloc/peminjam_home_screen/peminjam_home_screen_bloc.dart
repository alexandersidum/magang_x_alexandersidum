import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:magang_javan/api/book_repository.dart';
import 'package:magang_javan/model/book.dart';

part 'peminjam_home_screen_event.dart';
part 'peminjam_home_screen_state.dart';

class PeminjamHomeScreenBloc extends Bloc<PeminjamHomeScreenEvent, PeminjamHomeScreenState> {
  PeminjamHomeScreenBloc(this._bookRepository) : super(PeminjamHomeScreenState());
  final BookRepository _bookRepository;
  @override
  Stream<PeminjamHomeScreenState> mapEventToState(
    PeminjamHomeScreenEvent event,
  ) async* {
    if(event is LoadBook){
      yield* _loadBookByPeminjam(event.isPublish);
    }
    if(event is RentBook){
      yield* _rentBook(event.book,event.userId);
    }
  }

  Stream<PeminjamHomeScreenState> _loadBookByPeminjam(int isPublish) async*{
    yield state.copyWith(
      isLoading: true,
    );
    try{
      print("Loading book");
      List<Book> listBook = await _bookRepository.loadPublishedBook(isPublish);
      print("abisl ist book");
      yield state.copyWith(isLoading: false, listBook: listBook);
    }
    catch(e){
      print("CATCHED Loading book $e");
      yield state.copyWith(isLoading: false);
    }
  }

  Stream<PeminjamHomeScreenState> _rentBook(Book book, int userId) async*{
    yield state.copyWith(
      isLoading: true,
    );
    try{
      print("Loading book");
      bool isSuccess = await _bookRepository.editBook(book.copyWith(peminjamId: userId), null);
      print("abisl ist book");
      yield state.copyWith(isLoading: false);
    }
    catch(e){
      print("CATCHED Loading book $e");
      yield state.copyWith(isLoading: false);
    }
  }

}
