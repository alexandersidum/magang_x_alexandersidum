import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:magang_javan/model/book.dart';
import 'api_endpoint.dart';

class BookRepository {
  Dio _dio = Dio(BaseOptions(
    connectTimeout: 30 * 1000,
    followRedirects: false,
    contentType: "application/json",
    validateStatus: (status) {
      return status < 500;
    },
  ));

  Future<List<Book>> loadBookByPemilikId(int userId) async {
    try {
      Response res =
          await _dio.get(bookEndPoint, queryParameters: {"pemilikId": userId});
      print(res.data);
      if (res.statusCode == 201) {
        List<Book> output =
            List.from(res.data["data"]).map((e) => Book.fromMap(e)).toList();
        return output;
      } else {
        throw Exception(List.from(res.data["messages"])[0]);
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<List<Book>> loadPublishedBook(int isPublish) async {
    try {
      Response res =
          await _dio.get(bookEndPoint, queryParameters: {"isPublished": isPublish});
      print(res.data);
      if (res.statusCode == 201) {
        List<Book> output =
            List.from(res.data["data"]).map((e) => Book.fromMap(e)).toList();
        return output;
      } else {
        throw Exception(List.from(res.data["messages"])[0]);
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<List<Book>> loadBookByPeminjamId(int userId) async {
    try {
      Response res =
          await _dio.get(bookEndPoint, queryParameters: {"peminjamId": userId});
      print(res.data);
      if (res.statusCode == 201) {
        List<Book> output =
            List.from(res.data["data"]).map((e) => Book.fromMap(e)).toList();
        return output;
      } else {
        throw Exception(List.from(res.data["messages"])[0]);
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<bool> postBook(Book book, File image) async {
    FormData itemFormData = FormData.fromMap({
      "image": await MultipartFile.fromFile(image.path),
      "data": jsonEncode(book.toMap())
    });
    _dio.options.contentType = 'multipart/form-data';
    try {
      Response res = await _dio.post(bookEndPoint, data: itemFormData);
      if (res.statusCode == 201) {
        print(res.data);
        return true;
      } else {
        print(res.data);
        return false;
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<bool> editBook(Book book, File image) async {
    print("masuk editbook");
    FormData itemFormData = image!=null?FormData.fromMap({
      "image": await MultipartFile.fromFile(image.path),
      "data": jsonEncode(book.toMap())
    }):FormData.fromMap({
      "data": jsonEncode(book.toMap())
    });
    print(book.toMap());
    _dio.options.contentType = 'multipart/form-data';
    try {
      print("masuk editbook try");
       Response res = await _dio.post(bookEndPoint, data: itemFormData);
      if (res.statusCode == 201) {
        print(res.data);
        return true;
      } else {
        print(res.data);
        return false;
      }
    } catch (e) {
      print(e);
      rethrow;
    }
  }

  Future<bool> deleteBook(int bookId) async {
    _dio.options.contentType = 'application/json';
    try {
       Response res = await _dio.delete(bookEndPoint, data: {
         "id":bookId
       });
      if (res.statusCode == 201) {
        print(res.data);
        return true;
      } else {
        print(res.data);
        return false;
      }
    } catch (e) {
      rethrow;
    }
  }

  Future<List<Genre>> getGenre() async {
    try {
      Response res =
          await _dio.get(bookEndPoint, queryParameters: {"genre": 1});
      print(res.data);
      if (res.statusCode == 201) {
        List<Genre> output =
            List.from(res.data["data"]).map((e) => Genre.fromMap(e)).toList();
        return output;
      } else {
        throw Exception(List.from(res.data["messages"])[0]);
      }
    } catch (e) {
      rethrow;
    }
}
}