import 'package:dio/dio.dart';
import 'package:magang_javan/model/user.dart';
import 'api_endpoint.dart';

class AuthRepository {
  Dio _dio = Dio(BaseOptions(
    connectTimeout: 30 * 1000,
    followRedirects: false,
    contentType: "application/json",
    validateStatus: (status) {
      return status < 500;
    },
  ));

  Future<User> login(String email, String password) async {
    try {
      print("repologin");
      Response res = await _dio
          .post(signInEndPoint, data: {"email": email, "password": password});
      if(res.statusCode==201){
        print(res.data);
        return User.fromMap(res.data["data"]);
      }
      else{
        print(res.data);
        //Message from backend is list
        String message = List.from(res.data["messages"])[0];
        throw Exception(message);
      }
    } catch (e) {
      print("catched");
      print(e);
      rethrow;
    }
  }

  Future<bool> register(
      {String email,
      String password,
      String nama,
      String alamat,
      int role,
      String telepon}) async {
    try {
    // $query->bindParam(':email', $jsonData->email, PDO::PARAM_STR);
    // $query->bindParam(':nama', $jsonData->nama, PDO::PARAM_STR);
    // $query->bindParam(':password', $hashed_password, PDO::PARAM_STR);
    // $query->bindParam(':roles_id', $jsonData->roles_id, PDO::PARAM_INT);
    // $query->bindParam(':no_telepon', $jsonData->no_telepon, PDO::PARAM_STR);
    // $query->bindParam(':alamat', $jsonData->alamat, PDO::PARAM_STR);
      print("reporegis");
      Response res = await _dio
          .post(signUpEndPoint, data: {
            "email": email, 
            "password": password,
            "nama":nama,
            "roles_id":role,
            "no_telepon":telepon,
            "alamat":alamat
            });
      if(res.statusCode==200){
        print(res.data);
        return true;
      }
      else{
        print(res.data);
        //Message from backend is list
        String message = List.from(res.data["messages"])[0];
        throw Exception(message);
      }
    } catch (e) {
      print("catched");
      print(e);
      rethrow;
    }
  }
}
