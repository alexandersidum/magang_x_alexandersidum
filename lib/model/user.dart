import 'dart:convert';

import 'package:flutter/material.dart';

//ROLE 1 : PEMINJAM
//ROLE 2 : PEMILIK

class User {
  final int ID; 
  final String email; 
  final String no_telpon; 
  final String alamat; 
  final int roles_id;
  User({
    @required this.ID,
    @required this.email,
    @required this.no_telpon,
    @required this.alamat,
    @required this.roles_id,
  });

  User copyWith({
    int ID,
    String email,
    String no_telpon,
    String alamat,
    String roles_id,
  }) {
    return User(
      ID: ID ?? this.ID,
      email: email ?? this.email,
      no_telpon: no_telpon ?? this.no_telpon,
      alamat: alamat ?? this.alamat,
      roles_id: roles_id ?? this.roles_id,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'ID': ID,
      'email': email,
      'no_telpon': no_telpon,
      'alamat': alamat,
      'roles_id': roles_id,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
      ID: int.parse(map['ID']),
      email: map['email'],
      no_telpon: map['no_telpon'],
      alamat: map['alamat'],
      roles_id: int.parse(map['roles_id']),
    );
  }

  String toJson() => json.encode(toMap());

  factory User.fromJson(String source) => User.fromMap(json.decode(source));

  @override
  String toString() {
    return 'User(ID: $ID, email: $email, no_telpon: $no_telpon, alamat: $alamat, roles_id: $roles_id)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is User &&
      other.ID == ID &&
      other.email == email &&
      other.no_telpon == no_telpon &&
      other.alamat == alamat &&
      other.roles_id == roles_id;
  }

  @override
  int get hashCode {
    return ID.hashCode ^
      email.hashCode ^
      no_telpon.hashCode ^
      alamat.hashCode ^
      roles_id.hashCode;
  }
}
