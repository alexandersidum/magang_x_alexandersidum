import 'package:magang_javan/api/api_endpoint.dart';
class Book {
  // ID 	genre_id 	judul_buku 	deskripsi 	harga 	gambar 	pemilik_id 	peminjam_id 	status 	is_publish
  final int id;
  final String judulBuku;
  final String deskripsi;
  final int harga;
  final int genreId;
  final String genreName;
  final String gambar;
  final int pemilikId;
  final String pemilikName;
  final int peminjamId;
  final String peminjamName;
  final int status;
  final bool isPublish;

  Book(
      {this.id,
      this.genreId,
      this.genreName,
      this.pemilikName,
      this.peminjamName,
      this.judulBuku,
      this.deskripsi,
      this.harga,
      this.gambar,
      this.pemilikId,
      this.peminjamId,
      this.status,
      this.isPublish});

  //   $book['id'] = $this->getId();
  // $book['judulBuku'] = $this->getJudulBuku();
  // $book['deskripsi'] = $this->getDeskripsi();
  // $book['harga'] = $this->getHarga();
  // $book['gambar'] = $this->getGambar();
  // $book['genreId'] = $this->getGenreId();
  // $book['genreName'] = $this->getGenreName();
  // $book['pemilikId'] = $this->getPemilikId();
  // $book['pemilikName'] = $this->getPemilikName();
  // $book['peminjamId'] = $this->getPeminjamId();
  // $book['peminjamName'] = $this->getPeminjamName();
  // $book['status'] = $this->getStatus();
  // $book['isPublish'] = $this->getIsPublish();
  factory Book.fromMap(Map<String, dynamic> map) {
    return Book(
      id: int.parse(map['id']),
      judulBuku: map['judulBuku'],
      deskripsi: map['deskripsi'],
      harga: int.parse(map['harga']),
      gambar: baseEndPoint+map['gambar'],
      pemilikId: int.parse(map['pemilikId']),
      peminjamId: map['peminjamId']!=null?int.parse(map['peminjamId']):null,
      status: int.parse(map['status']),
      isPublish: int.parse(map['isPublish']) == 1,
      genreId: int.parse(map['genreId']),
      genreName: map['genreName'],
      pemilikName: map['pemilikName'],
      peminjamName: map['peminjamName'],
    );
  }

  Map<String, dynamic> toMap(){
    return {
      "bookId":this.id,
      "genreId":this.genreId,
      "judulBuku":this.judulBuku,
      "deskripsi":this.deskripsi,
      "harga":this.harga,
      "pemilikId":this.pemilikId,
      "peminjamId":this.peminjamId,
      "status":this.status,
      "isPublish":this.isPublish
    };
  }

  Book copyWith({
    int id,
    String judulBuku,
    String deskripsi,
    int harga,
    int genreId,
    String genreName,
    String gambar,
    int pemilikId,
    String pemilikName,
    int peminjamId,
    String peminjamName,
    int status,
    bool isPublish,
  }) {
    return Book(
      id: id ?? this.id,
      judulBuku: judulBuku ?? this.judulBuku,
      deskripsi: deskripsi ?? this.deskripsi,
      harga: harga ?? this.harga,
      genreId: genreId ?? this.genreId,
      genreName: genreName ?? this.genreName,
      gambar: gambar ?? this.gambar,
      pemilikId: pemilikId ?? this.pemilikId,
      pemilikName: pemilikName ?? this.pemilikName,
      peminjamId: peminjamId ?? this.peminjamId,
      peminjamName: peminjamName ?? this.peminjamName,
      status: status ?? this.status,
      isPublish: isPublish ?? this.isPublish,
    );
  }
}

class Genre {
  final int id;
  final String genreName;

  Genre(this.id, this.genreName);

  factory Genre.fromMap(data){
    return Genre(
      int.parse(data["ID"]),
      data["genre"]
    );
  }
}
