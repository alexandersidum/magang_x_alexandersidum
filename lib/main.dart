import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:magang_javan/bloc/auth_bloc/auth_bloc.dart';
import 'package:magang_javan/router/route_name.dart';
import 'package:magang_javan/router/router.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context)=>AuthBloc(),
      child: MaterialApp(
        title: 'Magang Javan',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        onGenerateRoute: generateRoute,
      ),
    );
  }
}

