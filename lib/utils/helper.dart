import 'package:flutter/material.dart';

bool kisPortrait(BuildContext context) {
  return MediaQuery.of(context).orientation == Orientation.portrait;
}

bool kisLandscape(BuildContext context) {
  return MediaQuery.of(context).orientation == Orientation.landscape;
}

double kScreenHeight(BuildContext context) {
  return MediaQuery.of(context).size.height;
}

kScreenWidth(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

