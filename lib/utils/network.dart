import 'dart:io';
import 'dart:math';
import "package:dio/dio.dart";
import 'package:path_provider/path_provider.dart';

Future<File> urlToFile(String imageUrl) async {
    var rng = Random();
    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    File file = File('$tempPath' + (rng.nextInt(100)).toString() + '.png');
    Response response = await Dio().get(imageUrl, options: Options(responseType: ResponseType.bytes));
    return await file.writeAsBytes(response.data);
  }